# Changelog for Maven-Parent

## [v1.2.0] - 2024-04-03

### Bug
* update maven-javadoc-plugin to from 2.6.8 to 3.6.3. #25877
* update groovy-all plugin dependency from 3.0.0-beta to 3.0.13 #25872

### Features
* new maven profiles for managing jdk7 (gcore-legacy) and jdk17 build (see #24259)
* add new group repositories for managing jdk17 dependencies resolution
* update groovy-maven and javadoc plugin configuration #25108
* add classpath external folder. see #25106

### Bug
* removed set-local-classpath profile. see #26841

## [v1.1.0] - 2022-10-27

### Features 
* force java 1.8 on jdk11 activation profiles
* update maven-compiler-plugin to 3.10.1
* New build profiles to support CI/CD
* add new execution phase to buildnumber-maven-plugin
* Enforcement for:
    * Java 8 (target and source)
    * OpenJDK as target Java VM.
    * Maven 3.3.9+

## [v1.0.0] - 2017-12-20

