# MAVEN PARENT POM

A parent pom for all the gCube artifacts.

# Enforcements 
* Java 8 (target and source) unless otherwise specified inside the pom
* OpenJDK as target Java VM.
* Maven 3.3.9+
* README.md, CHANGELOG.md and LICENSE.md are present in the baseDir of each Maven module
* CHANGELOG.md must declare a tag [v+Pom version] (e.g. [v1.0.0])

# Build Profiles

The build profiles are separated in three main branches: developer, staging, releases.
Each branch has a set of profile based on the idk.
The developer branch contains the profiles related to the snapshot build:

* gcore-legacy jdk7
* gcube-legacy - jdk8
* gcube-official -jdk11
* gcube-experimental - jdk17

In the same way are organised the other branch profiles related to the staging (integration) phase and to the release phase.

In addition there are a set of common profiles useful in order to avoid replication of plugins and rules definitions:

* common-snapshot
* common-release
* dry-run
* disable-java8-doclint
* set-local-classpath
* generate-distribution-packages

Below the descriptions of the most used profiles by gCube components:

## common-snapshot
This profile defines the common plugin used by all the snapshot profiles

## common-releases
This profile defines the common plugin used by all the staging and release profiles

## gcube-official
* Dependencies resolved against local, gcube-snapshots and gcube-releases
* Snapshot artifacts installed to local
* Snapshot artifacts deployed to gcube-snapshots
* Deployments of releases artifacts are not permitted

## jenkins-staging-official
* Dependencies resolved against local-staging, gcube-jenkins-staging, gcube-staging and gcube-releases
* Release/snapshot artifacts installed to local-staging
* Release/snapshot artifacts deployed to gcube-jenkins-staging

## jenkins-releases
* Dependencies resolved against local-releases, gcube-releases, gcube-jenkins-staging and gcube staging
* Release artifacts installed to local-releases
* Deployments of snapshot artifacts are not permitted
* Release artifacts deployed to gcube-releases

## dry-run
This profile disables the deployment of the artifacts. 

## disable-java8-doclint
This profile sets an additional parameter for javadoc generation to disables the doclint. 
It avoids the build fails if formal/syntax errors are found in javadoc comments.

## set-local-classpath
This profile set a generic folder that will be added to the classpath

